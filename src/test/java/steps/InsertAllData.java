package steps;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import drivers.Chrome;
import org.openqa.selenium.support.PageFactory;
import pages.BasePage;
import pages.CreateAccount;
import pages.LoginToPage;

import java.util.List;

import static drivers.Config.BASE_URL;

public class InsertAllData {


    @Given("^User am on homepage$")
    public void iAmOnHomepage() {
        BasePage.driver = Chrome.loadChrome();
        BasePage.driver.get(BASE_URL);
    }

    @And("^User click post Icon$")
    public void iClickPostIcon() {
        BasePage page = PageFactory.initElements(BasePage.driver, BasePage.class);
        page.pageLoad();
        page.confirmButton();
        page.getColorBeforeMoveOnIncon();
        page.moveOnIconPostIcon();
        page.getColorAfterMoveOnIncon();
        page.clickOnPostIcon();
    }


    @When("^User click 'załóż konto'$")
    public void iClickZałóżKonto() {
        LoginToPage loginPage = PageFactory.initElements(BasePage.driver, LoginToPage.class);
        loginPage.getTexDecBeforeMoveOnCreateAccountInscription();
        loginPage.moveOnCreateAccountInscription();
        loginPage.getTextDecAfterMoveOnCreateAccountInscription();
        loginPage.clickOnCreateAccountInscription();
    }

    @Then("^User see Registration Page with title 'Załóż konto w WP Poczcie'$")
    public void iSeeRegistrationPageWithTitleZałóżKontoWWPPoczcie() throws InterruptedException {
        CreateAccount createaccount = PageFactory.initElements(BasePage.driver, CreateAccount.class);
        createaccount.getTitle();
    }


    @When("^I fill first name  and last Name$")
    public void iFillFirstNameAndLastName(DataTable table) throws InterruptedException {
        CreateAccount createaccount = PageFactory.initElements(BasePage.driver, CreateAccount.class);
        createaccount.enterFirstSecondName(table);
    }

    @And("^User chose gender$")
    public void userChoseGender() {
        CreateAccount createaccount = PageFactory.initElements(BasePage.driver, CreateAccount.class);
        createaccount.choseGender();
    }

    @And("^User chose Day,Month,Year of brith$")
    public void userChoseDayMonthYearOfBrith(DataTable table) {
        CreateAccount createaccount = PageFactory.initElements(BasePage.driver, CreateAccount.class);
        createaccount.choseDayMonthYear(table);
    }

    @And("^User Chose Login$")
    public void userChoseLogin(List<String> list) {
        CreateAccount createaccount = PageFactory.initElements(BasePage.driver, CreateAccount.class);
        createaccount.choseLogin(list);

    }

    @And("^User fill password  repeat password$")
    public void userFillPasswordRepeatPassword(DataTable table) {
        CreateAccount createaccount = PageFactory.initElements(BasePage.driver, CreateAccount.class);
        createaccount.enterPassword(table);
    }

    @And("^User fill phone number$")
    public void userFillPhoneNumber(List<String> list) {
        CreateAccount createaccount = PageFactory.initElements(BasePage.driver, CreateAccount.class);
        createaccount.enterPhoneNumber(list);
    }

    @And("^User accept approval$")
    public void userAcceptApproval() {
        CreateAccount createaccount = PageFactory.initElements(BasePage.driver, CreateAccount.class);
        createaccount.acceptAproval();
    }

    @And("^User creates an account$")
    public void userCreatesAnAccount() {
        CreateAccount createaccount = PageFactory.initElements(BasePage.driver, CreateAccount.class);
        createaccount.createAccount();
    }

    @Then("^User see red warning title under Captcha label$")
    public void userSeeRedWarningTitleUnderCaptchaLabel() {
        CreateAccount createaccount = PageFactory.initElements(BasePage.driver, CreateAccount.class);
        createaccount.checkMessageUnderCaptcha();
    }
}
