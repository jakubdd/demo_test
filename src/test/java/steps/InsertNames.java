package steps;

import cucumber.api.java.en.Then;
import org.openqa.selenium.support.PageFactory;
import pages.BasePage;
import pages.CreateAccount;

public class InsertNames {

    @Then("^I fill fist name as \"([^\"]*)\"  and last name as \"([^\"]*)\"$")
    public void iFillFistNameAsAndLastNameAs(String firstname, String lastname) {
        CreateAccount createaccount = PageFactory.initElements(BasePage.driver, CreateAccount.class);
        createaccount.enterFirstSecondName(firstname, lastname);
    }
}
