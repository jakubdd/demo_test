package steps;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.*;
import pages.BasePage;
import pages.CreateAccount;

import java.util.concurrent.TimeUnit;

import static drivers.Chrome.loadChrome;
import static drivers.Config.CREATE_ACCOUNT_URL;
import static drivers.Firefox.closeFirefox;
import static drivers.Firefox.loadFirefox;


public class InsertPhoneNumberTest extends CreateAccount {


    @DataProvider(name = "inputs")
    public Object[][] getData() {
        return new Object[][]{
                {"000000000", true},
                {"000000000", true},
                {" 0         0 ", false},
                {"---------", false},
                {"!@#$%^&*!", false},
                {"/ą123456789", false},
                {"\"\\\\\\;-----(\"-----\");//\"", false},
                {"!@#$%^&*!", false}

        };
    }

    @BeforeClass
    @Parameters({"browser"})
    public void initializeDriver(String browser) {

        if (browser.equalsIgnoreCase("firefox")) {
            BasePage.driver = loadChrome();
        } else if (browser.equalsIgnoreCase("chrome")) {
            BasePage.driver = loadFirefox();
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(CREATE_ACCOUNT_URL);
        JavascriptExecutor jse = (JavascriptExecutor) BasePage.driver;
        jse.executeScript("window.scrollBy(0, 600);");
    }

    // ! -- TEST DON'T WORK PROPERLY ON FIREFOX AFTER UPDATE ON VERSION 59.0 -- !

    @Test(dataProvider = "inputs")
    public void inputCorretValuesChrome(String value, boolean result) {
        CreateAccount create = PageFactory.initElements(BasePage.driver, CreateAccount.class);
        create.enterPhoneNumber(value);  // 1. move click 2. send value

        Actions actions = new Actions(BasePage.driver);
        create.leftSideMoveClick(); // click on left site to exit window

        create.phoneMoveClick(); //4. back to form

        actions.sendKeys(Keys.chord(Keys.CONTROL, "a"));
        actions.sendKeys(Keys.BACK_SPACE);
        actions.build().perform(); // 5. clear value

        Assert.assertSame(phoneNumber, phoneNumber);

    }
    // ! -- TEST DON'T WORK PROPERLY ON FIREFOX AFTER UPDATE ON VERSION 59.0 -- !


    @AfterClass
    public void tearDown() {
        //  closeChrome(BasePage.driver);
        closeFirefox(BasePage.driver);
    }
}
