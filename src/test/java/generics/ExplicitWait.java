package generics;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ExplicitWait {

    public WebElement waitForElement(WebDriver driver, WebElement element, int time) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, time);
            wait.until(ExpectedConditions.elementToBeClickable(element));
        } catch (NoSuchElementException e) {
            System.out.println(":: No such element");
        }
        return element;
    }

    public WebElement waitForElements(WebDriver driver, List<WebElement> list, int time) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, time);
            wait.until(ExpectedConditions.elementToBeClickable((By) list));
        } catch (NoSuchElementException e) {
            System.out.println(":: No such element");
        }
        return (WebElement) list;
    }
}
