package generics;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class MoveClick {

    public void moveClickElementTable(WebDriver driver, WebElement element, int sec, List<String> list, int numberList) {

        ExplicitWait wait = new ExplicitWait();
        Actions actions = new Actions(driver);

        try {
            wait.waitForElement(driver, element, sec);
            actions.moveToElement(element).click();
            actions.sendKeys(list.get(numberList));
            actions.build().perform();
        } catch (NoSuchElementException e) {
            System.out.println(":: No such element");
        }

    }

    public void moveClickElementValue(WebDriver driver, WebElement element, int sec, String value) {

        ExplicitWait wait = new ExplicitWait();
        Actions actions = new Actions(driver);

        try {
            wait.waitForElement(driver, element, sec);
            actions.moveToElement(element).click();
            actions.sendKeys(value);
            actions.build().perform();
        } catch (NoSuchElementException e) {
            System.out.println(":: No such element");
        }
    }

    public void moveClick(WebDriver driver, WebElement element) {

        ExplicitWait wait = new ExplicitWait();
        Actions actions = new Actions(driver);

        try {
            wait.waitForElement(driver, element, 10);
            actions.moveToElement(element).click();
            actions.build().perform();
        } catch (NoSuchElementException e) {
            System.out.println(":: No such element");
        }
    }
}


