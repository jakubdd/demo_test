package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class LoginToPage extends BasePage {

    @FindBy(xpath = "//*[@id=\'loginForm\']/div[2]/a")
    public WebElement createAccountInscription;

    @FindBy(css = "#loginForm > div.konto > a")
    public WebElement createAccountInscriptionCss;


    public void getTexDecBeforeMoveOnCreateAccountInscription() {
        wait.waitForElement(driver, createAccountInscriptionCss, 10);
        String textDecoration = createAccountInscriptionCss.getCssValue("text-decoration");
        System.out.println("\n:: Before text-decoration: " + textDecoration);
    }

    public void moveOnCreateAccountInscription() {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        Actions action = new Actions(driver);
        wait.waitForElement(driver, createAccountInscription, 10);
        action.moveToElement(createAccountInscription).perform();
    }

    public void getTextDecAfterMoveOnCreateAccountInscription() {
        wait.waitForElement(driver, createAccountInscriptionCss, 10);
        String textDecoration = createAccountInscriptionCss.getCssValue("text-decoration");
        System.out.println(":: After text-decoration:  " + textDecoration);
    }

    public void clickOnCreateAccountInscription() {
        wait.waitForElement(driver, createAccountInscription, 10).click();
    }

}
