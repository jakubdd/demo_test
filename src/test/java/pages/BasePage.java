package pages;

import generics.ExplicitWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

public class BasePage {

    public static WebDriver driver;
    ExplicitWait wait = new ExplicitWait();

    @FindBy(xpath = "//*[@id='header']/header/div[1]/div/ul/li[1]/a")
    public WebElement postIcon;

    @FindBy(css = "#header > header > div.QJiSZl > div > ul > li:nth-child(1) > a")
    public WebElement postIconCss;

    @FindBy(xpath = "//*[@id=\'home-page-button\']/div/div")
    public WebElement disSetAsStartPage;

    public BasePage() {
    }

    public void pageLoad() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void confirmButton() {
        wait.waitForElement(driver, disSetAsStartPage, 10).click();
    }

    public void moveOnIconPostIcon() {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        Actions action = new Actions(driver);
        wait.waitForElement(driver, postIcon, 10);
        action.moveToElement(postIcon).perform();
    }

    public void getColorBeforeMoveOnIncon() {
        String color = postIconCss.getCssValue("color");
        System.out.println(":: Before color postIconCss: " + color);
    }

    public void getColorAfterMoveOnIncon() {
        String color = postIconCss.getCssValue("color");
        System.out.println(":: After color postIconCss: " + color);
    }

    public void clickOnPostIcon() {
        wait.waitForElement(driver, postIcon, 10).click();
    }
}

