package pages;

import cucumber.api.DataTable;
import generics.MoveClick;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.Map;

import static org.testng.Assert.assertTrue;

public class CreateAccount extends BasePage {

    @FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div/div[3]/form/div[1]/div[1]/div/input")
    public WebElement firstName;

    @FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div/div[3]/form/div[1]/div[2]/div/input")
    public WebElement lastName;

    @FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div/div[3]/form/div[2]/div/fieldset/div/div[1]/div/label")
    public WebElement buttonWomen;

    @FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div/div[3]/form/div[2]/div/fieldset/div/div[2]/div/label")
    public WebElement buttonMan;

    @FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div/div[3]/form/div[3]/fieldset/div/div[1]/input")
    public WebElement dayOfBirth;

    @FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div/div[3]/form/div[3]/fieldset/div/div[2]/div/select")
    public WebElement monthOfBrith;

    @FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div/div[3]/form/div[3]/fieldset/div/div[3]/input")
    public WebElement yearOfBrith;

    @FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div/div[3]/form/div[4]/div/div[1]/div[1]")
    public WebElement choseLogin;

    @FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div/div[3]/form/div[5]/div/div[1]/div[1]")
    public WebElement password;

    @FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div/div[3]/form/div[5]/div/div[2]/div")
    public WebElement repeatPassword;

    @FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div/div[3]/form/div[6]/div/fieldset/div")
    public WebElement phoneNumber;

    @FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div/div[3]/form/div[6]/div/fieldset/div")
    public WebElement phoneNumberError;

    @FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div/div[3]/form/div[7]/div/div[1]/label")
    public WebElement selectAllConsents;

    @FindBy(xpath = "//*[@id='recaptcha-anchor']/div[5]")
    public WebElement captcha;

    @FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div/div[3]/form/button")
    public WebElement createAccountButton;

    @FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div/div[3]/form/div[8]/div/ul/li")
    public WebElement incscriptionUnderCaptcha;

    @FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div/div[2]")
    public WebElement leftPageSite;


    public void getTitle() {
        pageLoad();
        assertTrue(driver.getTitle().contains("Załóż konto w WP Poczcie"));
    }

    public void enterFirstSecondName(DataTable table) {
        List<Map<String, String>> data = table.asMaps(String.class, String.class);
        wait.waitForElement(driver, firstName, 10);
        firstName.sendKeys(data.get(0).get("firstName"));
        wait.waitForElement(driver, lastName, 10);
        lastName.sendKeys(data.get(0).get("lastName"));
    }

    public void enterFirstSecondName(String firstnameArg, String lastnameArg) {
        wait.waitForElement(driver, firstName, 10);
        firstName.sendKeys(firstnameArg);
        wait.waitForElement(driver, lastName, 10);
        lastName.sendKeys(lastnameArg);

    }

    public void choseGender() {
        wait.waitForElement(driver, buttonMan, 10);
        buttonMan.click();
    }

    public void choseDayMonthYear(DataTable table) {
        List<Map<String, String>> data = table.asMaps(String.class, String.class);
        Select choseMonthDropDownList = new Select(monthOfBrith);

        wait.waitForElement(driver, dayOfBirth, 10);
        dayOfBirth.sendKeys(data.get(0).get("day"));

        wait.waitForElement(driver, monthOfBrith, 10);
        monthOfBrith.click();
        choseMonthDropDownList.selectByVisibleText(data.get(0).get("month"));

        wait.waitForElement(driver, yearOfBrith, 10);
        yearOfBrith.sendKeys(data.get(0).get("year"));
    }

    public void choseLogin(List<String> list) {
        MoveClick click = new MoveClick();
        click.moveClickElementTable(driver, choseLogin, 10, list, 1);
    }
    public void enterPassword(DataTable table) {
        List<Map<String, String>> data = table.asMaps(String.class, String.class);
        Actions actions = new Actions(driver);

        wait.waitForElement(driver, password, 10);
        actions.moveToElement(password).click();
        actions.sendKeys(data.get(0).get("password"));
        actions.build().perform();

        actions.moveToElement(repeatPassword).click();
        wait.waitForElement(driver, repeatPassword, 10);
        actions.sendKeys(data.get(0).get("repeatPassword"));
        actions.build().perform();

    }

    public void enterPhoneNumber(List<String> list) {

        MoveClick click = new MoveClick();
        click.moveClickElementTable(driver, phoneNumber, 10, list, 1);
    }

    public void enterPhoneNumber(String value) {
        MoveClick click = new MoveClick();
        click.moveClickElementValue(driver, phoneNumber, 10, value);
    }

    public void acceptAproval() {
        wait.waitForElement(driver, selectAllConsents, 10);
        selectAllConsents.click();
    }

    public void createAccount() {
        wait.waitForElement(driver, createAccountButton, 10);
        createAccountButton.click();
    }

    public void checkMessageUnderCaptcha() {
        wait.waitForElement(driver, incscriptionUnderCaptcha, 10);
        String TrueText = "Proszę wypełnić pole";
        String Text = incscriptionUnderCaptcha.getText();
        if (TrueText.equals(Text)) {
            System.out.println("Text under captcha(true): " + Text);
        }
    }

    public void phoneMoveClick() {
        MoveClick move = new MoveClick();
        move.moveClick(driver, phoneNumber);
    }

    public void leftSideMoveClick() {
        MoveClick move = new MoveClick();
        move.moveClick(driver, leftPageSite);
    }

}
