Feature: Registration on page

  Possible registration using the correct data.
  Impossible registration using the correct data.
  # https://cucumber.io/docs/reference

  Scenario: Insert registration with valid credencials expect captcha
    Given User am on homepage
    And User click post Icon
    When User click 'załóż konto'
    Then User see Registration Page with title 'Załóż konto w WP Poczcie'
    When I fill first name  and last Name
      | firstName |  | lastName |
      | Jan       |  | Kowalski |
    And User chose gender
    And User chose Day,Month,Year of brith
      | day |  | month    |  | year |
      | 10  |  | Sierpień |  | 1996 |
    And User Chose Login
      | login                |
      | example-1.2_34567890 |
    And  User fill password  repeat password
      | password      |  | repeatPassword |
      | zxcvbnm123!@# |  | zxcvbnm123!@#  |
    And User fill phone number
      | phoneNumber |
      | 123456789   |
    And User accept approval
    And User creates an account
    Then User see red warning title under Captcha label

  Scenario Outline: Insert first name and second name with valid credencials
    Given User am on homepage
    And User click post Icon
    When User click 'załóż konto'
    Then User see Registration Page with title 'Załóż konto w WP Poczcie'
    Then I fill fist name as "<firstname>"  and last name as "<lastname>"
    Examples:
      | firstname | lastname |
      | Bill      | Whitmore |
      | Margaret  | Farewell |
      | Adam      | Nowak    |




