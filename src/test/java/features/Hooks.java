package features;

import cucumber.api.java.After;
import pages.BasePage;

import static drivers.Chrome.closeChrome;


public class Hooks {

    @After
    public void tearDown() {
        closeChrome(BasePage.driver);
    }
}
