package drivers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class Firefox {

    public static WebDriver loadFirefox() {

        FirefoxOptions disable = new FirefoxOptions();
        disable.addArguments("--disable-notifications");

        System.setProperty("webdriver.gecko.driver", Config.FIREFOX_PATH);
        FirefoxDriver driver = new FirefoxDriver(disable);

        driver.manage().window().maximize();

        return driver;
    }

    public static void closeFirefox(WebDriver driver) {
        try {
            driver.quit();
        } catch (Exception e) {
            System.out.println(":: WebDriver stop error");
        }
        System.out.println(":: WebDriver stopped");
    }
}
