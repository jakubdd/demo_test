package drivers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Chrome {

    public static WebDriver loadChrome() {
        ChromeOptions disable = new ChromeOptions();
        disable.addArguments("--disable-notifications");

        System.setProperty("webdriver.chrome.driver", Config.CHROME_PATH);
        ChromeDriver driver = new ChromeDriver(disable);

        driver.manage().window().maximize();

        return driver;
    }

    public static void closeChrome(WebDriver driver) {
        try {
            driver.quit();
        } catch (Exception e) {
            System.out.println(":: WebDriver stop error");
        }
        System.out.println(":: WebDriver stopped");
    }
}





