package drivers;

public class Config {
    public static final String BASE_URL = "https://wp.pl/";
    public static final String CHROME_PATH = "C:\\Users\\Jakub\\Desktop\\QA\\wp_test\\src\\main\\resources\\chromedriver.exe";
    public static final String FIREFOX_PATH = "C:\\Users\\Jakub\\Desktop\\QA\\wp_test\\src\\main\\resources\\geckodriver.exe";
    public static final String IE_PATH = "C:\\Users\\Jakub\\Desktop\\QA\\wp_test\\src\\main\\resources\\IEDriverServer.exe";
    public static final String CREATE_ACCOUNT_URL = "https://nowyprofil.wp.pl/rejestracja/?idu=99&serwis=nowa_poczta_wp&url=https%3A%2F%2Fpoczta.wp.pl%2Findexgwt.html%3Fflg%3D1";
}
